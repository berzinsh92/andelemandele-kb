## AndeleMandele tech task installation
Make sure docker is up and running.
1. `docker compose build`
2. `docker compose up -d`
3. `docker compose exec app composer install`
4. `docker compose exec app cp .env.example .env`
5. `docker compose exec app php artisan key:generate`
6. `docker compose exec app chmod -R 777 storage bootstrap/cache`

## Run these commands outside container for faster building
7. `npm install`
8. `npm run build`