<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('tv-show/rick-and-morty')->group(function () {
    Route::get('characters', [\App\Http\Controllers\Api\RickAndMortyApiController::class, 'characters']);
    Route::get('character/{id}', [\App\Http\Controllers\Api\RickAndMortyApiController::class, 'character']);
});