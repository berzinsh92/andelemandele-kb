<?php
namespace App\Services\RickAndMorty\Dto;

use App\Services\RickAndMorty\Model\CharacterLocation;
use Illuminate\Support\Arr;

class RickAndMortyCharacterLocationDto {

    public function create(array $data = []): CharacterLocation
    {
        $characterLocation = new CharacterLocation();
        $characterLocation->setName(Arr::get($data, 'name'));
        $characterLocation->setUrl(Arr::get($data, 'url'));

        return $characterLocation;
    }

}