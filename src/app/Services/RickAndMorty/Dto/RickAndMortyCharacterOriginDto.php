<?php
namespace App\Services\RickAndMorty\Dto;

use App\Services\RickAndMorty\Model\CharacterOrigin;
use Illuminate\Support\Arr;

class RickAndMortyCharacterOriginDto {

    public function create(array $data = []): CharacterOrigin
    {
        $characterOrigin = new CharacterOrigin();
        $characterOrigin->setName(Arr::get($data, 'name'));
        $characterOrigin->setUrl(Arr::get($data, 'url'));

        return $characterOrigin;
    }

}