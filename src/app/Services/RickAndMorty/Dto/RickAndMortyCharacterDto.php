<?php
namespace App\Services\RickAndMorty\Dto;

use App\Services\RickAndMorty\Model\Character;
use App\Services\RickAndMorty\Model\CharacterLocation;
use App\Services\RickAndMorty\Model\CharacterOrigin;
use Illuminate\Support\Arr;

class RickAndMortyCharacterDto {

    public function create(array $data = []): Character
    {
        $characterOrigin = new CharacterOrigin();
        $characterOrigin->setName(Arr::get($data, 'origin.name'));
        $characterOrigin->setUrl(Arr::get($data, 'origin.url'));

        $characterLocation = new CharacterLocation();
        $characterLocation->setName(Arr::get($data, 'location.name'));
        $characterLocation->setUrl(Arr::get($data, 'location.url'));

        $character = new Character();
        $character->setId(Arr::get($data, 'id'));
        $character->setName(Arr::get($data, 'name'));
        $character->setStatus(Arr::get($data, 'status'));
        $character->setSpecies(Arr::get($data, 'species'));
        $character->setType(Arr::get($data, 'type'));
        $character->setGender(Arr::get($data, 'gender'));
        $character->setOrigin($characterOrigin);
        $character->setLocation($characterLocation);
        $character->setImage(Arr::get($data, 'image'));
        $character->setEpisode(Arr::get($data, 'episode'));
        $character->setUrl(Arr::get($data, 'url'));
        $character->setCreated(Arr::get($data, 'created'));

        return $character;
    }

}