<?php
namespace App\Services\RickAndMorty\Model;

use Illuminate\Support\Arr;

class Character implements \JsonSerializable {

    private int $id;

    private string $name;

    private string $status;

    private string $species;

    private string $type;

    private string $gender;

    private CharacterOrigin $origin;

    private CharacterLocation $location;

    private string $image;

    private array $episode;

    private string $url;

    private string $created;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Character
     */
    public function setId(int $id): Character
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Character
     */
    public function setName(string $name): Character
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Character
     */
    public function setStatus(string $status): Character
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecies(): string
    {
        return $this->species;
    }

    /**
     * @param string $species
     * @return Character
     */
    public function setSpecies(string $species): Character
    {
        $this->species = $species;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Character
     */
    public function setType(string $type): Character
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return Character
     */
    public function setGender(string $gender): Character
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return CharacterOrigin
     */
    public function getOrigin(): CharacterOrigin
    {
        return $this->origin;
    }

    /**
     * @param CharacterOrigin $origin
     * @return Character
     */
    public function setOrigin(CharacterOrigin $origin): Character
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return CharacterLocation
     */
    public function getLocation(): CharacterLocation
    {
        return $this->location;
    }

    /**
     * @param CharacterLocation $location
     * @return Character
     */
    public function setLocation(CharacterLocation $location): Character
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Character
     */
    public function setImage(string $image): Character
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return array
     */
    public function getEpisode(): array
    {
        return $this->episode;
    }

    /**
     * @param array $episode
     * @return Character
     */
    public function setEpisode(array $episode): Character
    {
        $this->episode = $episode;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Character
     */
    public function setUrl(string $url): Character
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return Character
     */
    public function setCreated(string $created): Character
    {
        $this->created = $created;
        return $this;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}