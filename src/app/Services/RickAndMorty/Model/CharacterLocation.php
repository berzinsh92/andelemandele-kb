<?php
namespace App\Services\RickAndMorty\Model;

class CharacterLocation implements \JsonSerializable {

    /** @var string $name */
    private string $name;

    /** @var string $url */
    private string $url;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CharacterLocation
     */
    public function setName(string $name): CharacterLocation
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return CharacterLocation
     */
    public function setUrl(string $url): CharacterLocation
    {
        $this->url = $url;
        return $this;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

}