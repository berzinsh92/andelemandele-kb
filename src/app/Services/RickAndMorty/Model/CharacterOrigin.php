<?php
namespace App\Services\RickAndMorty\Model;

class CharacterOrigin implements \JsonSerializable {

    private string $name;
    private string $url;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CharacterOrigin
     */
    public function setName(string $name): CharacterOrigin
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return CharacterOrigin
     */
    public function setUrl(string $url): CharacterOrigin
    {
        $this->url = $url;
        return $this;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

}