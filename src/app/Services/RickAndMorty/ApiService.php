<?php
namespace App\Services\RickAndMorty;

use App\Exceptions\RickAndMortyApiException;
use Illuminate\Http\Client\Response;

abstract class ApiService {

    /**
     * @throws RickAndMortyApiException
     */
    public function handleErrors(Response $response)
    {
        if (get_class($this) === RickAndMortyApiService::class) {
            if ($response->failed()) {
                throw new RickAndMortyApiException($response->json('error') ?? 'Error reason is not provided', $response->status());
            }
        }
    }

}