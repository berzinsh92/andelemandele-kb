<?php

namespace App\Services\RickAndMorty;

use App\Services\RickAndMorty\Factory\CharacterResponseFactory;
use App\Services\RickAndMorty\Factory\CharactersResponseFactory;
use Illuminate\Support\Facades\Http;

class RickAndMortyApiService extends ApiService {

    /** @var string $url  */
    protected string $url;

    public function __construct()
    {
        $this->url = config('api.rick-and-morty');
    }

    /**
     * getCharacters: used to get all the characters of Rick and Morty.
     * @throws \App\Exceptions\RickAndMortyApiException
     */
    public function getCharacters(int $page = 1): CharactersResponseFactory
    {
        $args = [
            'page' => $page,
        ];

        $response = Http::get($this->url . '/character', $args);
        $this->handleErrors(response: $response);

        return CharactersResponseFactory::create(response: $response);
    }

    /**
     * getCharacter: used to get single character of Rick and Morty.
     *
     * @param int $id
     * @return CharacterResponseFactory
     * @throws \App\Exceptions\RickAndMortyApiException
     */
    public function getCharacter(int $id): CharacterResponseFactory
    {
        $response = Http::get($this->url . '/character/' . $id);
        $this->handleErrors(response: $response);

        return CharacterResponseFactory::create(response: $response);
    }

}