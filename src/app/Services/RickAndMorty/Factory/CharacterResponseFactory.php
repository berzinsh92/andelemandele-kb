<?php
namespace App\Services\RickAndMorty\Factory;

use App\Services\RickAndMorty\Model\Character;
use \Illuminate\Http\Client\Response;

class CharacterResponseFactory
{
    /** @var Character $data */
    public Character $data;

    /**
     * @param Response $response
     * @return static
     */
    public static function create(Response $response): static
    {
        $characterFactoryObj = new static();
        $characterFactoryObj->data = CharacterFactory::create(data: $response->json());

        return $characterFactoryObj;
    }

}