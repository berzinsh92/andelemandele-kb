<?php
namespace App\Services\RickAndMorty\Factory;

use App\Services\RickAndMorty\Dto\RickAndMortyCharacterDto;
use App\Services\RickAndMorty\Model\Character;

class CharacterFactory {

    /**
     * @param array $data
     * @return Character
     */
    public static function create(array $data = []): Character
    {
        $characterDto = new RickAndMortyCharacterDto();
        return $characterDto->create(data: $data);
    }

}