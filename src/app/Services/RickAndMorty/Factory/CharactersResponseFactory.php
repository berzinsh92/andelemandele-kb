<?php
namespace App\Services\RickAndMorty\Factory;

use App\Services\RickAndMorty\Model\Character;
use \Illuminate\Http\Client\Response;

class CharactersResponseFactory
{
    /** @var array $info */
    public array $info;

    /** @var Character[] $data */
    public array $data;

    /**
     * @param Response $response
     * @return static
     */
    public static function create(Response $response): static
    {
        $characterFactoryObj = new static();
        $characterFactoryObj->info = $response->json('info');

        foreach ($response->json('results') as $item) {
            $characterFactoryObj->data[] = CharacterFactory::create(data: $item);
        }

        return $characterFactoryObj;
    }

}