<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\RickAndMortyCharacters;
use App\Services\RickAndMorty\RickAndMortyApiService;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class RickAndMortyApiController extends BaseController{

    protected RickAndMortyApiService $rickAndMortyApi;

    public function __construct(RickAndMortyApiService $rickAndMortyApiService)
    {
        $this->rickAndMortyApi = $rickAndMortyApiService;
    }

    public function characters(RickAndMortyCharacters $request): JsonResponse
    {
        $page = $request->get('page') ?? 1;
        $characters = $this->rickAndMortyApi->getCharacters(page: $page);

        return response()->json($characters);
    }

    public function character(int $id): JsonResponse
    {
        $character = $this->rickAndMortyApi->getCharacter(id: $id);

        return response()->json($character);
    }

}