<?php
namespace Tests\Feature;

use Tests\TestCase;

class HomeControllerTest extends TestCase {

    public function testHasHomeViewSuccess()
    {
        $response = $this->get('/');
        $response->assertViewIs('home');
    }

}