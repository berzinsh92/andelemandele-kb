<?php
namespace Tests\Feature\Api;
use App\Exceptions\RickAndMortyApiException;
use Tests\TestCase;

class RickAndMortyApiControllerTest extends TestCase
{
    /**
     * A basic functional test example.
     */
    public function testGetCharactersSuccess(): void
    {
        $response = $this->getJson('/api/tv-show/rick-and-morty/characters', ['page' => '1']);
        $response
            ->assertStatus(200)
            ->assertJsonIsArray(['data'])
            ->assertJsonIsObject(['info']);
    }

    public function testGetCharacterSuccess(): void
    {
        $id = 2;
        $response = $this->getJson('/api/tv-show/rick-and-morty/character/' . $id);
        $response
            ->assertStatus(200)
            ->assertJsonIsObject(['data']);
        $this->assertEquals($id, $response->json('data.id'));
        $this->assertEquals('Morty Smith', $response->json('data.name'));
    }
}