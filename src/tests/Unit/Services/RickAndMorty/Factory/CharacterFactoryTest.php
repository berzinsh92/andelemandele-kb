<?php
namespace Tests\Unit\Services\RickAndMorty\Factory;

use App\Services\RickAndMorty\Factory\CharacterFactory;
use App\Services\RickAndMorty\Model\Character;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class CharacterFactoryTest extends TestCase {

    use WithFaker;

    /** @covers \App\Services\RickAndMorty\Factory\CharacterFactory::create */
    public function testCreateSuccess()
    {
        $data = [];
        Arr::set($data, 'origin.name', $this->faker->word);
        Arr::set($data, 'origin.url', $this->faker->word);
        Arr::set($data, 'location.name', $this->faker->word);
        Arr::set($data, 'location.url', $this->faker->word);

        Arr::set($data, 'id', $this->faker->randomDigit());
        Arr::set($data, 'name', $this->faker->name);
        Arr::set($data, 'status', $this->faker->word);
        Arr::set($data, 'species', $this->faker->word);
        Arr::set($data, 'type', $this->faker->word);
        Arr::set($data, 'gender', $this->faker->word);
        Arr::set($data, 'image', $this->faker->imageUrl);
        Arr::set($data, 'episode', $this->faker->words());
        Arr::set($data, 'url', $this->faker->url);
        Arr::set($data, 'created', $this->faker->time);

        $c = CharacterFactory::create($data);
        $this->assertInstanceOf(Character::class, $c);
        $this->assertEquals(Arr::get($data, 'origin.name'), $c->getOrigin()->getName());
        $this->assertEquals(Arr::get($data, 'origin.url'), $c->getOrigin()->getUrl());
        $this->assertEquals(Arr::get($data, 'location.name'), $c->getLocation()->getName());
        $this->assertEquals(Arr::get($data, 'location.url'), $c->getLocation()->getUrl());

        $this->assertEquals(Arr::get($data, 'id'), $c->getId());
        $this->assertEquals(Arr::get($data, 'name'), $c->getName());
        $this->assertEquals(Arr::get($data, 'status'), $c->getStatus());
        $this->assertEquals(Arr::get($data, 'species'), $c->getSpecies());
        $this->assertEquals(Arr::get($data, 'type'), $c->getType());
        $this->assertEquals(Arr::get($data, 'gender'), $c->getGender());
        $this->assertEquals(Arr::get($data, 'image'), $c->getImage());
        $this->assertEquals(Arr::get($data, 'episode'), $c->getEpisode());
        $this->assertEquals(Arr::get($data, 'url'), $c->getUrl());
        $this->assertEquals(Arr::get($data, 'created'), $c->getCreated());
    }

}