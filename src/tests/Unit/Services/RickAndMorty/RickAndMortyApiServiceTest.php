<?php
namespace Tests\Unit\Services\RickAndMorty;

use App\Exceptions\RickAndMortyApiException;
use App\Services\RickAndMorty\Factory\CharacterResponseFactory;
use App\Services\RickAndMorty\Factory\CharactersResponseFactory;
use App\Services\RickAndMorty\RickAndMortyApiService;
use Tests\TestCase;

class RickAndMortyApiServiceTest extends TestCase {

    /**
     * @covers \App\Services\RickAndMorty\RickAndMortyApiService::getCharacters
     */
    public function testGetCharactersSuccess()
    {
        $page = 2;
        $c = new RickAndMortyApiService();

        $response = $c->getCharacters($page);

        $this->assertInstanceOf(CharactersResponseFactory::class, $response);
        $this->assertIsObject($response);
        $this->assertObjectHasProperty('info', $response);
        $this->assertObjectHasProperty('data', $response);

        foreach ($response->data as $item) {
            $this->assertObjectHasProperty('id', $item);
            $this->assertObjectHasProperty('name', $item);
            $this->assertObjectHasProperty('status', $item);
            $this->assertObjectHasProperty('species', $item);
            $this->assertObjectHasProperty('type', $item);
            $this->assertObjectHasProperty('gender', $item);
            $this->assertObjectHasProperty('origin', $item);
            $this->assertObjectHasProperty('location', $item);
            $this->assertObjectHasProperty('image', $item);
            $this->assertObjectHasProperty('episode', $item);
            $this->assertObjectHasProperty('url', $item);
            $this->assertObjectHasProperty('created', $item);
        }
    }

    /**
     * @covers \App\Services\RickAndMorty\RickAndMortyApiService::getCharacter
     */
    public function testGetCharacterSuccess()
    {
        $id = 2;
        $c = new RickAndMortyApiService();
        $response = $c->getCharacter($id);

        $this->assertInstanceOf(CharacterResponseFactory::class, $response);
        $this->assertEquals($id, $response->data->getId());
        $this->assertEquals('Morty Smith', $response->data->getName());
        $this->assertEquals('Male', $response->data->getGender());
    }

    /**
     * @covers \App\Services\RickAndMorty\RickAndMortyApiService::handleErrors
     */
    public function testHandleErrorsSuccess()
    {
        $id = 1002300;
        $c = new RickAndMortyApiService();

        $this->assertThrows(fn() => ($c->getCharacter($id)), RickAndMortyApiException::class, 'Character not found');
    }

}