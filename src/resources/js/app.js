import './bootstrap';
import {createApp} from 'vue'
import { createStore } from 'vuex'
import { createRouter, createWebHistory } from 'vue-router'

import App from './RickAndMorty/App.vue'
import CharacterItem from "./RickAndMorty/CharacterItem.vue";
import CharacterList from "./RickAndMorty/CharacterList.vue";

// Routes
const routes = [
    { path: '/', name: 'home', component: CharacterList },
    { path: '/character/:id', name: 'character', component: CharacterItem },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
})

// Store
const store = createStore({
    state () {
        return {
            rickAndMorty: {
                characters: [],
            }
        }
    },
    mutations: {
        setRickAndMortyCharacters (state, data) {
            state.rickAndMorty.characters = data;
        },
        addRickAndMortyCharacters (state, data) {
            data.forEach((item) => {
                state.rickAndMorty.characters.push(item);
            });
        }
    },
    getters: {
        getCharacterById: (state) => (id) => {
            return state.rickAndMorty.characters.find(character => character.id === id);
        }
    }
});

const app = createApp(App);
app.use(store);
app.use(router);
app.mount("#app");