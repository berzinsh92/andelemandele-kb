import { ref } from 'vue'

export async function useFetch(url) {
    let data = ref(null);
    let error = ref(null);

    await axios.get(url).then((response) => {
        data = response.data;
    }).catch((err) => {
        error = err;
    });

    return {data, error}
}